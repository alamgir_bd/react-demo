import React from 'react';
import {BrowserRouter as Router,
Switch,
Route,
Link
} from "react-router-dom";
import logo from './logo.svg';
import './App.css';
import { domainToASCII } from 'url';

function App() {
  return (
   <Router>
     <div>
       <nav>
         <ul>
           <li>
             <Link to="/">Home</Link>
           </li>
         </ul>
       </nav>
     </div>
   </Router>
  );
}

export default App;
